import { Layout } from '@/components/dom/Layout'
import '@/global.css'
import dynamic from 'next/dynamic'
import 'bootstrap/dist/css/bootstrap.css';


export const metadata = {
  title: 'Text3D | CodeVerse',
  description: 'Afficher un texte 3D avec react-three-fiber three.js nextjs sans javascriptAfficher un texte 3D avec react-three-fiber three.js nextjs sans javascriptAfficher un texte 3D avec react-three-fiber three.js nextjs sans javascript',
}

export default function RootLayout({ children }) {
  return (
    <html lang='en'>
      {/*
        <head /> will contain the components returned by the nearest parent
        head.tsx. Find out more at https://beta.nextjs.org/docs/api-reference/file-conventions/head
      */}
      <head />
      <body>
        {/* To avoid FOUT with styled-components wrap Layout with StyledComponentsRegistry https://beta.nextjs.org/docs/styling/css-in-js#styled-components */}
        <Layout>{children}</Layout>
      </body>
    </html>
  )
}
